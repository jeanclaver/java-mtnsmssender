package mtn.com.conde.service.sms;

import java.io.IOException;

import javax.xml.soap.*;
import mtnz.util.logging.*;

//import org.apache.log4j.Logger;

public class SIMSWAPAPI {
	
	//private static Logger logger = Logger.getLogger(SIMSwapService.class);
 
    public boolean  runSimswap( String NEW_SIM_NUMBER , String ENTITY_ID , String EFFECTIVE_FROM , String EXTERNAL_SYSTEMS_LOG_REFERNCE, String url) 
    {
    	
    	try{
    	 // Create SOAP Connection
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        // Send SOAP Message to SOAP Server
        //String url = " http://192.168.33.151:8011/weca_proxy?wsdl";
        SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(NEW_SIM_NUMBER, ENTITY_ID,EFFECTIVE_FROM,EXTERNAL_SYSTEMS_LOG_REFERNCE ), url);

        // print SOAP Response
        System.out.print("Response SOAP Message:");
        soapResponse.writeTo(System.out);

        soapConnection.close();
        
    	}
    	catch(Exception ex)
    	{
    		MLogger.Log(this,  ex);
    		
    		return false;
    	}
    	
    	return  true;
    }

    private static SOAPMessage createSOAPRequest( String NEW_SIM_NUMBER , String ENTITY_ID , String EFFECTIVE_FROM , String EXTERNAL_SYSTEMS_LOG_REFERNCE) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String soapenvURI = "http://schemas.xmlsoap.org/soap/envelope/";
        String comURI="http://schema.concierge.com";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("soapenv", soapenvURI);
        envelope.addNamespaceDeclaration("com", comURI);
        envelope.addNamespaceDeclaration("ser", comURI);

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        SOAPElement com = soapBody.addChildElement("clientRequest", "com");

        SOAPElement EaiEnvelope = com.addChildElement("EaiEnvelope");
        
                    SOAPElement ApplicationName = EaiEnvelope.addChildElement("ApplicationName");
                                ApplicationName.addTextNode("MTNGN");
                                
                    SOAPElement Domain = EaiEnvelope.addChildElement("Domain");
                                Domain.addTextNode("abl_portal");
                                
                                SOAPElement Service = EaiEnvelope.addChildElement("Service");
                                Service.addTextNode("SIMSWAPService");
                                
                                SOAPElement Language = EaiEnvelope.addChildElement("Language");
                                Language.addTextNode("En");
                                
                                
                                SOAPElement UserId = EaiEnvelope.addChildElement("UserId");
                                UserId.addTextNode("externalapp");
                                
                                SOAPElement Sender = EaiEnvelope.addChildElement("Sender");
                                Sender.addTextNode("externalapp");
                                
                                
                                SOAPElement MessageId = EaiEnvelope.addChildElement("MessageId");
                                MessageId.addTextNode("abl_portal");
                                
                                SOAPElement Payload = EaiEnvelope.addChildElement("Payload");
               
                                  // Payload NODE
                                SOAPElement ser_Services= Payload.addChildElement("Services","ser");
                                SOAPElement ser_Request= ser_Services.addChildElement("Request","ser");
                                
                                SOAPElement ser_Operation_Name= ser_Request.addChildElement("Operation_Name","ser");
                                ser_Operation_Name.addTextNode("abillityReferenceApi");
                                
                                SOAPElement ser_ChangeServicesRequest= ser_Request.addChildElement("ChangeServicesRequest","ser");
                                
                                SOAPElement ser_Request2=ser_ChangeServicesRequest.addChildElement("request","ser");
                                
                                SOAPElement EVENT = ser_Request2.addChildElement("EVENT");
                                
                                SOAPElement  REQUEST = EVENT.addChildElement("REQUEST");
                                REQUEST.setAttribute("API_CODE", "7001");
                                
                                REQUEST.setAttribute("ENTITY_ID",ENTITY_ID ); // mobile number 
                               // REQUEST.setAttribute("ENTITY_ID", "000019914"); // mobile number 

                                
                                REQUEST.setAttribute("EXTERNAL_APPLICATION", "admin");
                                
                                REQUEST.setAttribute("EXTERNAL_REFERENCE", "");
                                
                                REQUEST.setAttribute("EXTERNAL_USER", "admin");

                                REQUEST.setAttribute("INFO_LEVEL", "");

                                REQUEST.setAttribute("CLIENT_ID", "MTNG");
                                
                                REQUEST.setAttribute("OPERATION_NAME", "simchange");

                                REQUEST.setAttribute("EXTERNAL_SYSTEMS_LOG_REFERNCE", EXTERNAL_SYSTEMS_LOG_REFERNCE ); // to be change yyyymmddhhmmss 20110705122056
                                
                                //REQUEST.setAttribute("EXTERNAL_SYSTEMS_LOG_REFERNCE", ""); // to be change yyyymmddhhmmss 20110705122056

                                
                                SOAPElement  REQUESTDETAILS = EVENT.addChildElement("REQUESTDETAILS");
                                
                                REQUESTDETAILS.setAttribute("REQUEST_TYPE", "SIMC");

                                REQUESTDETAILS.setAttribute("COMMENTS", "SIMSWAP MTN APPLICATION DONE BY IT MC");


                                REQUESTDETAILS.setAttribute("EFFECTIVE_FLAG", "I");

                                REQUESTDETAILS.setAttribute("EFFECTIVE_FROM", EFFECTIVE_FROM); // to be change 
                                //REQUESTDETAILS.setAttribute("EFFECTIVE_FROM", "2017-05-08T00:00:00.000Z"); // to be change 

                                
                                REQUESTDETAILS.setAttribute("FLAG", "W");
                                
                                REQUESTDETAILS.setAttribute("NEW_CATEGORY", "");
                                
                                //REQUESTDETAILS.setAttribute("NEW_SIM_NUMBER", "892240415072230028"); // NEW SIM to be change
                                REQUESTDETAILS.setAttribute("NEW_SIM_NUMBER", NEW_SIM_NUMBER); // NEW SIM to be change

                                REQUESTDETAILS.setAttribute("PAYMENT_TYPE", "N");
                                
                                REQUESTDETAILS.setAttribute("REASON_CODE", "01");
                                
                                REQUESTDETAILS.setAttribute("SIM_CHANGE_TYPE", "N");
                                
                                REQUESTDETAILS.setAttribute("TRANSACTION_AMOUNT", "");

                                
        MimeHeaders headers = soapMessage.getMimeHeaders();

        soapMessage.saveChanges();
        System.out.print("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println();
        
        //logger.debug(System.out );

        return soapMessage;
    }

}