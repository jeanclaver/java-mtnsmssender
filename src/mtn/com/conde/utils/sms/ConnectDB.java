package mtn.com.conde.utils.sms;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;


public class ConnectDB {

    public Connection conn;
    public Statement sttmnt;
    public String users="";

    private static Logger logger = Logger.getLogger(ConnectDB.class);

    
    
    public static void main( String [] arge) throws SQLException, ClassNotFoundException
    //cbsdbon
    {
    	//ConnectDB ab;	
  // ConnectDB ab = new ConnectDB("jdbc:oracle:thin:@scancbscluster:1521:cbsdbon2","BIB_VIEW","BIB_VIEW");
 //ConnectDB ab = new ConnectDB("jdbc:oracle:thin:@192.168.44.45:1521:cbsdbon1","BIB_VIEW","BIB_VIEW");
//ConnectDB ab = new ConnectDB("jdbc:oracle:thin:@10.12.255.45:1521:imconfig","USSD_APP","ussd_app");
ConnectDB ab =new ConnectDB("jdbc:oracle:thin:@10.12.255.45:1521:imconfig","USSD_APP","ussd_app");
 //ConnectDB ab = new ConnectDB("jdbc:oracle:thin:@192.168.33.140:1521:CBSDBUAT","CBS_VIEW_UAT","CBS_VIEW_UAT");
    	System.out.println(" Test COnnectivity Good ");
    	
    	
    	
    	//
    }
    
    
    public ConnectDB( String url ,  String login , String pwd) throws SQLException, ClassNotFoundException{
    	
    	//"jdbc:oracle:thin:@192.168.1.86:1521:prm","prm","prm"
    	//Class.forName("oracle.jdbc.OracleDriver" );
        DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver() );
	    conn = DriverManager.getConnection (url,login,pwd);
	    
	    //creation d'un statement 
	    if( conn != null )
        sttmnt =  conn.createStatement();


	    System.out.println(url);
	    
	    logger.debug(url);	
    }
    
    
    public  boolean closeCOnnexion()
    {
    	try{
    		
    		if(sttmnt!= null) sttmnt.close();
    		if(conn != null ) conn.close();
    		
    		return true ;
    	}
    	catch(Exception ex )
    	{
    		ex.printStackTrace();
    		return false ;
    	}
    	
    }
    

    public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public void setSttmnt(Statement sttmnt) {
		this.sttmnt = sttmnt;
	}

	public void setUsers(String users) {
		this.users = users;
	}

	public Statement getSttmnt() {
        return sttmnt;
    }

    public ResultSet  selectQuery( String query )
    {
    	//System.out.println("***** La requete **********");
    	
    	System.out.println(query);
    	
    	logger.debug(query);	
    	 ResultSet result = null; 
    	try{
    		sttmnt =  conn.createStatement();
    		result = sttmnt.executeQuery(query);
    		//sttmnt.close();
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    		
        	logger.debug(ex.getMessage());	

    	}
    	
    	//if(result == null) System.out.println(" ******* Sortie  de crise ******");
    	
    	return result ;
    }

    public int  updateRequet( String query )
    {
    	
    	System.out.println(query);
    	
    	logger.debug(query);	
    	 int result = -1; 
    	try{
    		sttmnt =  conn.createStatement();
    		result = sttmnt.executeUpdate(query);
    		//sttmnt.close();
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    		
        	logger.debug(ex.getMessage());	

    	}
    	
    	//if(result == null) System.out.println(" ******* Sortie  de crise ******");
    	
    	return result ;
    }


	public String getUsers() {
		return users;
	}

        

}
