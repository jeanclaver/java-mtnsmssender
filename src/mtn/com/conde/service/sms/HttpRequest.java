package mtn.com.conde.service.sms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpRequest {

	private static final String USER_AGENT = "";

	private static final String GET_URL = "";

	private static final String POST_URL = "";

	private static final String POST_PARAMS = "";
	
	
	//private static String url2 = "http://10.13.7.145:9001/smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME=smart&PASSWORD=Smart@123&MOBILENO=224664222542&MESSAGE=MERCIDIABY&ORIGIN_ADDR=MTNS&TYPE=0";
	private static String url2 ;
	//= "http://10.13.7.145:9001/smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME=smart&PASSWORD=Smart@123&MOBILENO=224664222542&MESSAGE=MERCIDIABY&ORIGIN_ADDR=MTNS&TYPE=0";

	private String ip;
	
	private String USERNAME;
	
	private String PASSWORD;
	
	private String MOBILENO;
	
	private String MESSAGE;
	
	public static void main(String[] args) throws IOException {

		//sendGET();
		System.out.println("GET DONE");
		//sendPOST();
		//System.out.println("POST DONE");
		
		//http://10.13.255.139:9001/smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME=sswap&PASSWORD=Root@123&MOBILENO=224664222518&MESSAGE=Fode&ORIGIN_ADDR=MTNGC&TYPE=0
//		HttpRequest.sendGET("http://10.13.255.139:9001/smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME=sswap&PASSWORD=Root@123&MOBILENO=224664222546&MESSAGE=Test&ORIGIN_ADDR=MTNGC&TYPE=0");
                HttpRequest.sendGET("http://10.12.255.36:9001/smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME=sswap&PASSWORD=Root@123&MOBILENO=224664222544&MESSAGE=Hello Jean&ORIGIN_ADDR=MTNGC&TYPE=0");
                 //old http://10.13.255.141:9001/smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME=sswap&PASSWORD=Root@123&MOBILENO=2246644222544&MESSAGE=Test&ORIGIN_ADDR=corp&TYPE=0
                 
//                 smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME=smart&PASSWORD=Smart@123&MOBILENO=224664222505&MESSAGE=MamadouTest&ORIGIN_ADDR=MTNS&TYPE=0

		 
	}
	
	public HttpRequest()
	{
		 this.url2 = url2;
	}

	public  static void sendGET( String httpRequest) throws IOException {
		URL obj = new URL(httpRequest);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = con.getResponseCode();
		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
		} else {
			System.out.println("GET request not worked");
		}

	}

	private static void sendPOST() throws IOException {
		URL obj = new URL(POST_URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);

		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(POST_PARAMS.getBytes());
		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { //success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
		} else {
			System.out.println("POST request not worked");
		}
	}

}
