package mtn.com.conde.service.sms;


import mtn.com.conde.utils.sms.ConnectDB;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

//import mtn.conde.HttpRequest;

import org.apache.log4j.Logger;


public class SIMSwapService {
	
	//private ConnectDB dbTT;
	private ConnectDB dbVASCLOUD ; 
	//private String urlVsCloud = "10.12.255.45";
    private static Logger logger = Logger.getLogger(SIMSwapService.class);
    
	//private String url ; //= " http://192.168.33.151:8011/weca_proxy?wsdl";
	private String ipSMSC , portSMSC;
	private String USERNAMESMPP ;
	private String PASSWORDSMPP;
	private String MOBILENOSMPP;
	private String MESSAGESMPP;
	private ResultSet res ;
	
public static void main( String [] args) throws ClassNotFoundException, SQLException
{
	//SIMSwapService ns = new SIMSwapService();
	//ns.processSimSwap();
	//String url = " http://192.168.33.151:8011/weca_proxy?wsdl";
//	String url = "http://192.168.44.50:7003/weca_proxy?wsdl";
	
	String VCIP = "10.12.255.45" ;

	String VCUSER ="USSD_APP";//USSD_APP
	String VCLOG ="ussd_app" ; 
	String VCLSID = "imconfig";
	//String VCIP = "192.168.1.60" ;
	//String VCUSER ="M2M_USER";
	//String VCLOG ="m2muser" ; 
	//String VCLSID = "m2m";
//	String TTIP = "192.168.44.41";
//	String TTUSER ="BIB_VIEW";
//	String TTLOG ="BIB_VIEW"; 
//	String TTLSID  ="cbsdbon1";
//	String ipSMSC = "10.13.7.145";
        String ipSMSC ="10.12.255.36";
	String portSMSC="9001";
	String USERNAME = "sswap";
	String PASSWORD = "Root@123";

		try {
			SIMSwapService  simswap  = new SIMSwapService(VCIP,VCUSER,VCLOG,VCLSID,USERNAME,PASSWORD,ipSMSC,portSMSC);
			simswap.sendNotification();
			simswap.closeconnection();
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}
	/*
	 * private  ;
	private ;
	private String MOBILENOSMPP;
	private String MESSAGESMPP;
	 */
public  SIMSwapService( String VCIP, String VCUSER, String VCLOG, String VCLSID, String USERNAMESMPP, String PASSWORDSMPP, String ipSMSC, String portSMSC) throws ClassNotFoundException, SQLException
{
	  //dbVASCLOUD= new ConnectDB("jdbc:oracle:thin:@192.168.1.60:1521:m2m","M2M_USER","m2muser");
	  dbVASCLOUD= new ConnectDB("jdbc:oracle:thin:@"+VCIP+":1521:"+VCLSID,VCUSER,VCLOG);
	  
//	  this.url = url;

	 // dbVASCLOUD= new ConnectDB("jdbc:oracle:thin:@10.12.255.45:1521:imconfig","USSD_APP","ussd_app");

	  //dbTT =  new ConnectDB("jdbc:oracle:thin:@"+TTIP+":1521:"+TTLSID,TTUSER,TTLOG);
	 // dbTT new ConnectDB("jdbc:oracle:thin:@"+VCIP+":1521:"+VCLSID,VCUSER,VCLOG);
	   
	   
	   this.USERNAMESMPP= USERNAMESMPP;
	   this.PASSWORDSMPP = PASSWORDSMPP;
	   this.ipSMSC = ipSMSC;
	   this.portSMSC = portSMSC;
	  
	  logger.debug("Connection" );
}

    public void closeconnection()
    {
            if(dbVASCLOUD != null ){
                dbVASCLOUD.closeCOnnexion();
                //MLogger.Log(this, LogLevel.ALL, "VAS DB Connection is closing ");
            }
            
    }
	

	
	

	
	/**
	 * @throws SQLException 
	 * 
	 * 
	 */
	
	

	public void sendNotification() throws SQLException
	{
		//HttpRequest http = newHttpRequest  ()
                String tableName= "RXSENDER";
//            String tableName= "RXSENDER_TEST";
		
                //MLogger.Log(this, LogLevel.ALL," *********** sendNotification *****************");
		// check if the sender is Allowed
		String req1 = " UPDATE  "+tableName+"  SET SMS_FLAG=1  where SMS_FLAG = 0 " ;
		
		//MLogger.Log(this, LogLevel.ALL,req1);
	     
		// Change the  SMS FLAG
		dbVASCLOUD.updateRequet(req1);
		
		String req2 = " SELECT * FROM "+tableName+" WHERE  SMS_FLAG = 1  " ;
                //MLogger.Log(this, LogLevel.ALL,req2);
		 ResultSet res = dbVASCLOUD.selectQuery(req2);
		try{
			int i=1;
			while(res.next())
			{
                            //MLogger.Log(this, LogLevel.ALL,"Sending SMS ["+i+"]");
                            String RXFROM = res.getString(1);
				String gsm = res.getString(2);
				String message = URLEncoder.encode(res.getString(3), "UTF-8") ;	
				//MLogger.Log(this, LogLevel.ALL," IP : "+ipSMSC);
				//MLogger.Log(this, LogLevel.ALL," IP : "+ipSMSC);
                                //http://10.13.255.139:9001/smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME=sswap&PASSWORD=Root@123&MOBILENO=224664222546&MESSAGE=Pathe&ORIGIN_ADDR=MTNGC&TYPE=0
				String currentURL = "http://"+ipSMSC+":"+this.portSMSC+"/smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME="+USERNAMESMPP+"&PASSWORD="+PASSWORDSMPP+"&MOBILENO="+gsm+"&MESSAGE="+message+"&ORIGIN_ADDR="+RXFROM+"&TYPE=0";			
				
				MLogger.Log(this, LogLevel.ALL,"["+i+"] From "+RXFROM+" to "+gsm+" Message "+message);
				//HttpRequest.sendGET("http://10.13.7.145:9001/smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME=smart&PASSWORD=Smart@123&MOBILENO=224664222505&MESSAGE=GOOD&ORIGIN_ADDR=MTNS&TYPE=0");
//                                http://10.13.255.141:9001/smshttpquery/qs?REQUESTTYPE=SMSSubmitReq&USERNAME=sswap&PASSWORD=Root@123&MOBILENO=224664477180&MESSAGE=Test&ORIGIN_ADDR=corp&TYPE=0

				HttpRequest.sendGET(currentURL);
                                i++;
			}
			
		}
		catch( Exception ex )
		{
			MLogger.Log(this, ex);
			
		}
		finally {
			
			if( res!= null )
			res.close();
			//dbVASCLOUD.sttmnt.close();
			 
			if( dbVASCLOUD.sttmnt != null) dbVASCLOUD.sttmnt.close();
        }
		
       req1 = " UPDATE  "+tableName+"  SET SMS_FLAG=2 , DATE_SENT= sysdate where SMS_FLAG = 1 " ;
		
		//MLogger.Log(this, LogLevel.ALL,req1);
		// Change the  SMS FLAG
	   dbVASCLOUD.updateRequet(req1);
	   dbVASCLOUD.sttmnt.close();
		 
		if( dbVASCLOUD.sttmnt != null) dbVASCLOUD.sttmnt = null;
	     		
	}
	
	

	
}
