/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtnsmssender;

import java.io.File;
import mtn.conde.sms.live.SMSThread;
import mtnz.util.logging.MLogger;

/**
 *
 * @author Administrateur
 */
public class MTNSMSSender {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String filePath = "C:\\Logs\\MTNSMSSender";
        try{
            File file = new File(filePath);
            if (!file.exists()) {
                if (file.mkdir()) {
                    System.out.println("Directory is created!");
                } else {
                    System.out.println("Failed to create directory!");
                }            
            }
            MLogger.setHomeDirectory(filePath);
            
            
            try{
                SMSThread smchange = new SMSThread();
                smchange.start();
            }catch(Exception e){
                MLogger.Log(new MTNSMSSender(), e);
            }
        
        
        }catch(Exception e){
            e.printStackTrace();
        }
        
        

	
    }
    
}
